---
title: Keyboard Shortcuts
date: 2021-07-21T19:22:29+01:00
anchor: keyboard-shortcuts
weight: 340
---

When using an external keyboard, some keyboard shortcuts are
implemented:
 * Ctrl+E &ndash; Edit mode
 * Ctrl+Shift+E &ndash; View mode
 * Ctrl+N &ndash; New file
 * Ctrl+O &ndash; Open file
 * Ctrl+S &ndash; Save file
 * Ctrl+Shift+S &ndash; Save as
