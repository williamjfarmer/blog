---
title: Share
date: 2023-01-20T12:36:31Z
anchor: share
weight: 5000
---

Tap the **Share** button in the toolbar to share an image of the
current display.
