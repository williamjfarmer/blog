---
title: Keyboard Shortcuts
date: 2021-07-22T12:18:09+01:00
anchor: keyboard-shortcuts
weight: 280
---

When using an external keyboard, some keyboard shortcuts are
implemented:
 * Ctrl+A &ndash; Accept
 * Ctrl+E &ndash; Edit
 * Ctrl+N &ndash; New file
 * Ctrl+O &ndash; Open file
 * Ctrl+S &ndash; Save file
 * Ctrl+Shift+S &ndash; Save as
