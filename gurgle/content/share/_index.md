---
title: Share
date: 2022-02-12T19:07:37Z
anchor: share
weight: 550
---

### Image
Share an image of the current display.

### Code

![QR Code](images/Gurgle-code.png)

Display an alphanumeric code and a QR code which represents a new
sequence of words to guess. This code may be shared with another copy
of Gurgle on another device to set the same sequence of words. Tapping
the QR code button on the dialog shares the QR code image. Tapping the
code button shares the alphanumeric code.
