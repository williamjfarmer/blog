---
title: Features
date: 2020-03-04T10:11:56Z
anchor: features
weight: 110
---


 * Currency rates from [Floatrates][1]
 * 150 international currencies
 * Currency rates updated daily
 * Last rate update retained for use offline

[1]: https://www.floatrates.com
